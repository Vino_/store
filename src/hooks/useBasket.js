import {
  displayActionMessage,
  getDataFromLocalStorage,
  setDataToLocalStorage,
} from "helpers/utils";
import { useDispatch, useSelector } from "react-redux";
import {
  addToBasket as dispatchAddToBasket,
  updateToBastet,
  removeFromBasket,
} from "redux/actions/basketActions";
import { BASKET_DATA } from "../constants/constants";

const useBasket = () => {
  const { basket } = useSelector((state) => ({
    basket: getDataFromLocalStorage(BASKET_DATA)
      ? getDataFromLocalStorage(BASKET_DATA)
      : state.basket,
  }));
  const dispatch = useDispatch();

  const isItemOnBasket = (id) => !!basket.find((item) => item.id === id);

  const addToBasket = (product) => {
    if (isItemOnBasket(product.id)) {
      dispatch(removeFromBasket(product.id));
      let _list = getDataFromLocalStorage(BASKET_DATA)
        ? getDataFromLocalStorage(BASKET_DATA)
        : [];
      _list = _list.filter((a) => a.id !== product.id);
      setDataToLocalStorage(BASKET_DATA, _list);
      displayActionMessage("Item removed from basket", "info");
    } else {
      let _list = getDataFromLocalStorage(BASKET_DATA)
        ? getDataFromLocalStorage(BASKET_DATA)
        : [];
      setDataToLocalStorage(BASKET_DATA, [..._list, product]);
      dispatch(dispatchAddToBasket(product));
      displayActionMessage("Item added to basket", "success");
    }
  };

  const updateBasket = (id, updateItem) => {
    if (isItemOnBasket(id)) {
      let objIndex = basket.findIndex((obj) => obj.id == id);
      if (objIndex >= 0) {
        basket[objIndex] = { ...basket[objIndex], ...updateItem };
      }
      setDataToLocalStorage(BASKET_DATA, basket);
      dispatch(updateToBastet(basket));
      displayActionMessage("Basket Item Updated", "info");
    }
  };

  return { basket, isItemOnBasket, addToBasket, updateBasket };
};

export default useBasket;
