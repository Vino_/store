/* eslint-disable jsx-a11y/label-has-associated-control */
import { useFormikContext } from "formik";
import React from "react";

const HamsapayPayment = () => {
  const { values, setValues } = useFormikContext();

  return (
    <div className={`checkout-fieldset-collapse is-selected-payment`}>
      <div className="checkout-field margin-0">
        <div className="checkout-checkbox-field">
          <input
            checked={values.type === "hamsapay"}
            // checked={true}
            id="modeHamsapay"
            name="type"
            onChange={(e) => {
              if (e.target.checked) {
                setValues({ ...values, type: "hamsapay" });
              }
            }}
            type="radio"
          />
          <label className="d-flex w-100" htmlFor="modeHamsapay">
            <div className="d-flex-grow-1 margin-left-s">
              <h4 className="margin-0" style={{ fontSize: "18px" }}>
                Pay by Bank Transfer
              </h4>
              <span className="text-subtle d-block">Our Preferred method</span>
            </div>
            {/* <div className="payment-img payment-img-paypal" /> */}
            <div style={{ textAlign: "right" }}>
              <div className="text-subtle d-block">
                Secure, easy B2B transactions
              </div>
              <div className="text-subtle d-block">
                Powered by{" "}
                <span style={{ color: "#696982", fontSize: "inherit" }}>
                  Hamsapay{" "}
                </span>{" "}
              </div>
            </div>
            {/* <img
              src="/estore/static/logo-hamsapay.svg"
              style={{ height: "30px", objectFit: "contain" }}
            /> */}
          </label>
        </div>
      </div>
    </div>
  );
};

export default HamsapayPayment;
