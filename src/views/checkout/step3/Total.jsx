import { ArrowLeftOutlined, CheckOutlined } from "@ant-design/icons";
import { CHECKOUT_STEP_2, THANK_YOU } from "constants/routes";
import { useFormikContext } from "formik";
import {
  displayMoney,
  getDataFromLocalStorage,
  setDataToLocalStorage,
} from "helpers/utils";
import PropType from "prop-types";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { setPaymentDetails } from "redux/actions/checkoutActions";
import axios from "axios";
import { LoadingOutlined } from "@ant-design/icons";
import { displayActionMessage } from "helpers/utils";
import { v4 as uuidv4 } from "uuid";

const Total = ({ isInternational, subtotal, shipping }) => {
  const [loading, setLoading] = useState(false);

  const { values, submitForm } = useFormikContext();
  const history = useHistory();
  const dispatch = useDispatch();

  const onClickBack = () => {
    // destructure to only select left fields omitting cardnumber and ccv
    const { cardnumber, ccv, ...rest } = values;

    dispatch(setPaymentDetails({ ...rest })); // save payment details
    history.push(CHECKOUT_STEP_2);
  };

  const generateId = () => {
    // Math.random should be unique because of its seeding algorithm.
    // Convert it to base 36 (numbers + letters), and grab the first 9 characters
    // after the decimal.
    return uuidv4();
  };

  const confirmOrder = () => {
    if (values.type !== "hamsapay") {
      displayActionMessage("Feature not ready yet :)", "info");
    } else {
      setLoading(true);
      axios
        .post(
          // "https://sandbox-api.hamsapay.com/sandbox/api/v1/authorize",
          "https://qa-api.hamsapay.com/dev/api/v1/authorize",
          {},
          {
            headers: {
              client_id: "test_client1",
              client_secret:
                "$2a$10$BurTWIy5NTF9GJJH4magz.9Bd4bBurWYG8tmXxeQh1vs7r/wnCFG2",
            },
          }
        )
        .then((response) => {
          console.log("response->authorize", response);
          if (response.data && response.data.code === "success") {
            setDataToLocalStorage(
              "accessToken",
              response.data.data.accessToken
            );
            createPayment();
          } else {
            setLoading(false);
            displayActionMessage("Internal Server Error", "info");
          }
        })
        .catch((err) => {
          console.log("error", err);
          setLoading(false);
          displayActionMessage("Internal Server Error", "info");
        });
    }
  };

  const createPayment = () => {
    let names =
      shipping && shipping.fullname ? shipping.fullname.split(" ") : [];
    let _firstName = names[0] ? names[0] : "";
    let _lastName = names[1] ? names[1] : "";
    let email = shipping && shipping.email ? shipping.email : "";
    let timeStamp = new Date().getTime();
    axios
      .post(
        // "https://sandbox-api.hamsapay.com/sandbox/api/v1/payment/create",
        "https://qa-api.hamsapay.com/dev/api/v1/payment/create",

        {
          requestId: generateId(),
          merchantId: "72c2fc95a417904211c0fb7f8582f361",
          clientOrderId: `OrderNo-${timeStamp}`,
          amount: {
            amount: subtotal,
            currencyCode: "USD",
          },
          productCode: "BANK_PAYMENT",
          buyer: {
            id: "c2000098-d4d6-4cba-844f-2964897777f2",
          },
          customer: {
            firstName: _firstName,
            lastName: _lastName,
            email: email,
            country: "US",
            companyName: "Salinaka",
          },
          extendInfo: {
            sales: {
              id: "9d62e25e-6cd0-43d0-8408-f5d61e8c4c15",
            },
          },
        },
        {
          headers: {
            Authorization: "Bearer " + getDataFromLocalStorage("accessToken"),
          },
        }
      )
      .then((response) => {
        console.log("response->payment/create", response);
        if (response.data && response.data.code === "success") {
          // // history.push(THANK_YOU);
          // history.location(response.data.data.paymentUrl);
          window.location.replace(response.data.data.paymentUrl);
        } else {
          setLoading(false);
          displayActionMessage("Internal Server Error", "info");
        }
      })
      .catch((err) => {
        setLoading(false);
        console.log("error->payment/create", err);
        displayActionMessage("Internal Server Error", "info");
      });
  };

  return (
    <>
      <div className="basket-total text-right">
        <p className="basket-total-title">Total:</p>
        <h2 className="basket-total-amount">
          {displayMoney(subtotal + (isInternational ? 50 : 0))}
        </h2>
      </div>
      <br />
      <div className="checkout-shipping-action">
        <button
          className="button button-muted"
          onClick={() => onClickBack(values)}
          type="button"
        >
          <ArrowLeftOutlined />
          &nbsp; Go Back
        </button>
        <button
          className="button"
          disabled={loading}
          onClick={confirmOrder}
          type="button"
        >
          {loading ? <LoadingOutlined /> : <CheckOutlined />}
          &nbsp; Confirm
        </button>
      </div>
    </>
  );
};

Total.propTypes = {
  isInternational: PropType.bool.isRequired,
  subtotal: PropType.number.isRequired,
};

export default Total;
