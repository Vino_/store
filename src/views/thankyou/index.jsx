import { ArrowRightOutlined, ShopOutlined } from "@ant-design/icons";
import { BasketItem } from "components/basket";
import { CHECKOUT_STEP_2 } from "constants/routes";
import { displayMoney } from "helpers/utils";
import { useDocumentTitle, useScrollTop } from "hooks";
import PropType from "prop-types";
import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

const ThankYouPage = ({ basket, subtotal }) => {
  useDocumentTitle("Thank you | Salinaka");
  useScrollTop();
  const dispatch = useDispatch();
  const history = useHistory();
  const onClickPrevious = () => history.push("/");
  const onClickNext = () => history.push(CHECKOUT_STEP_2);

  return (
    <div className="checkout">
      <div className="checkout-step-1">
        <h3 className="text-center">Thank You for Your Order</h3>

        <br />
        <div className="checkout-shipping-action">
          <button
            className="button button-muted"
            onClick={onClickPrevious}
            type="button"
          >
            <ShopOutlined />
            &nbsp; Continue Shopping
          </button>
        </div>
      </div>
    </div>
  );
};

ThankYouPage.propTypes = {
};

export default ThankYouPage;
